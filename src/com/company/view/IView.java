package com.company.view;

import com.company.model.Pair;

import java.io.IOException;

public interface IView {
    interface View{
        void run() throws IOException;
        void message(String val);
        void choose(String val) throws IOException;
        void exit();
        void stopInnerCycle();
        void help();

        String getCalcType();
        String operation() throws IOException;
        int firstNumber() throws IOException;
        int secondNumber() throws IOException;
    }

    interface Presenter{
        void init(View view);
        void input(String string) throws IOException;
        void chooseFactory() throws IOException;

        void setInnerCycle(boolean b);
    }
}
