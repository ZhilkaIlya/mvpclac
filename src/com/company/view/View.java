package com.company.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View implements IView.View{
    private IView.Presenter presenter;
    public boolean innerCycle = true;
    private boolean flagExit = true;
    private BufferedReader reader;
    private String info = "Чтобы выбоать калькулятор, напишите - calc" + "\n"+
            "Чтобы выбрать конвертер, напишите - convert" + "\n" +
            "Если вам нужна помощь, напишите help" + "\n" +
            "Если вы хотите выйти, напишите exit";


    public View() {
        System.out.println(info);
        presenter = new Presenter();
        presenter.init(this);
    }

    @Override
    public void run() throws IOException {
        String str = "";
        while (flagExit){
            reader = new BufferedReader(new InputStreamReader(System.in));
            str = reader.readLine();
            presenter.input(str);
            run();
        }
    }

    @Override
    public void message(String val) {
        System.out.println(val);
    }

    @Override
    public void choose(String val) throws IOException {
        switch (val){
            case "calc":
                System.out.println("Вы выбрали калькулятор");
                presenter.chooseFactory();
                break;
            case "convert":
                break;
            case "help":
               help();
                break;
            case "exit":
                System.out.println("Вы выбрали exit");
                exit();
                break;
        }
    }

    @Override
    public void exit() {
        flagExit = false;
    }

    @Override
    public void stopInnerCycle() {
        innerCycle = false;
    }


    @Override
    public void help() {
        System.out.println(getInfo());
    }

    @Override
    public String getCalcType() {
        return "calc";
    }



    @Override
    public String operation() throws IOException {
        System.out.println("Вы можете выбрать только + - * / или help");
        String operation = "";
        String str = null;
        reader = new BufferedReader(new InputStreamReader(System.in));
        str = reader.readLine();
        if (str.equals("/") || str.equals("*") || str.equals("+") || str.equals("-")) {
            operation = str;
            return operation;
        }else if (str.equals("help")){
            System.out.println(getInfo());
            presenter.setInnerCycle(false);
            return str;
        }else {
            return operation();
        }
    }

    @Override
    public int firstNumber() throws IOException {
        System.out.println("Введите первое число");
        String str = "";
        reader = new BufferedReader(new InputStreamReader(System.in));
        str = reader.readLine();
        if (isInteger(str)){
            return Integer.parseInt(str);
        }else {
           return firstNumber();
        }
    }

    @Override
    public int secondNumber() throws IOException {
        System.out.println("Введите второе число");
        String str = "";
        reader = new BufferedReader(new InputStreamReader(System.in));
        str = reader.readLine();
        if (isInteger(str)){
            return Integer.parseInt(str);
        }else {
            return secondNumber();
        }
    }

    public String getInfo() {
        return info;
    }
    private static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }


}
