package com.company.view;

import com.company.model.Factory;
import com.company.model.IFactory;
import com.company.model.Pair;

import java.io.IOException;


public class Presenter implements IView.Presenter, IFactory.CallBack {
    private IView.View view;
    private Factory factory;
    private boolean innerCycle = true;
    private Pair pair = new Pair();
    public Presenter() {
         this.factory = Factory.getInstance();
    }

    @Override
    public void init(IView.View view) {
        this.view = view;

    }

    @Override
    public void input(String string) throws IOException {
        view.choose(string);
    }

    @Override
    public void chooseFactory() throws IOException {
        String operation = view.operation();
        if (innerCycle) {
            while (innerCycle) {
                String type = view.getCalcType();
                int firstNumber = view.firstNumber();
                int secondNumber = view.secondNumber();
                pair.setType(type);
                pair.setOper(operation);
                pair.setVal1(firstNumber);
                pair.setVal2(secondNumber);
                IFactory factory = Factory.getInstance().factory(this, new Pair<>(pair.getType(), pair.getOper(),
                        pair.getVal1(), pair.getVal2()));
                factory.calculation();
                chooseFactory();
            }
            innerCycle = true;
        }
    }

//    @Override
//    public Pair initCalculc() throws IOException {
////            String operation = view.operation();
////
////            int firstNumber = view.firstNumber();
////            int secondNumber = view.secondNumber();
////            pair.setType("calc");
////            pair.setOper(operation);
////            pair.setVal1(firstNumber);
////            pair.setVal2(secondNumber);
//            return pair;
//    }

    @Override
    public void message(String val) {
        System.out.println(val);
    }

    public void setInnerCycle(boolean innerCycle) {
        this.innerCycle = innerCycle;
    }
}
