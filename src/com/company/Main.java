package com.company;

import com.company.model.Pair;
import com.company.view.IView;
import com.company.view.Presenter;
import com.company.view.View;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        IView.View view = new View();
        view.run();
    }
}
