package com.company.model;

import com.company.view.IView;

import static com.company.model.Constant.TAG_CALC;
import static com.company.model.Constant.TAG_CONVERTOR;

public class Factory  {
    private IView.Presenter presenter;
    

    private static Factory instance;
    public static Factory getInstance() {
        if (instance != null)
            return instance;
        else
            return new Factory();
    }


    private Factory() {

    }

    public <TYPE,OPER,VAL1,VAL2>IFactory factory(IFactory.CallBack listener,Pair<TYPE,OPER,VAL1,VAL2> pair){
        switch ((String)pair.getType()){
            case TAG_CALC:
                return new Calc(listener,(Pair<String, String, Object, Object>) pair);
            case TAG_CONVERTOR :
                return new Convertor((Pair<String, String, Object, Object>) pair);
            default: throw new ArithmeticException();
        }
    }
}
