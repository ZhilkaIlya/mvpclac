package com.company.model;

public class Calc implements IFactory {
    private IFactory.CallBack listener;
    private Pair<String, String, Object, Object> pair;


    public Calc( IFactory.CallBack listener,Pair<String, String, Object, Object> pair) {
        this.listener=listener;
        this.pair = pair;

    }



    public void calculation() {
        int result = 0;

        switch (pair.getOper()) {
            case "+":
                result = (int) pair.getVal1() + (int) pair.getVal2();
                listener.message("You resolt :" + result);
                break;
            case "-":
                result = (int) pair.getVal1() - (int) pair.getVal2();
               listener.message("You resolt :" + result);
                break;
            case "*":
                result = (int) pair.getVal1() * (int) pair.getVal2();
               listener.message("You resolt :" + result);
                break;
            case "/":
                if ((int) pair.getVal2() == 0) {
                    result = 0;
                    listener.message("You result :" + result);
                    break;
                } else {
                    result = (int) pair.getVal1() / (int) pair.getVal2();
                    listener.message("You result :" + result);
                    break;
                }

        }
    }
}
